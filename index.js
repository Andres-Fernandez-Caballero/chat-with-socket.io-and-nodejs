const app = require('./app/app')
const http = require('http')

const server = http.createServer(app)
const port = 3000

const {Server} = require('socket.io')
const io = new Server(server)

const CONNECTION = 'connection'
const DISCONNECT = 'disconnect'
io.on(CONNECTION, socket => {
    console.log('a user was connected ' + socket.id)
    socket.on('chat message', (msgData) => {
        console.log(`message: ${msgData}`)
        io.emit('chat message', msgData)
    })
    socket.on(DISCONNECT, () => {
        console.log('user was disconnected')
        io.emit('chat message', {msg: 'user was disconnected'+ socket.id, id: socket.id})
    })
})

server.listen(port, ()=> {
    console.log('server listing on http://localhost:3000')
})

