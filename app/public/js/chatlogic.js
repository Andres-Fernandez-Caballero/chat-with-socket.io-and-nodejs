const socket = io();
const form = document.getElementById('form')
const input = document.getElementById('input')
const messagesContainer = document.getElementById('messages')

function createItem(msg, id) {
    const item = document.createElement('li')
    const container = document.createElement('div')
    container.classList.add('message')
    const message = document.createElement('p')
    message.classList.add('message-text')
    message.innerHTML = `<span>Mensaje: </span>${msg}`
    const user = document.createElement('p')
    user.classList.add('message-user')
    user.innerHTML = `<span>Usuario: </span>${id}`

    container.appendChild(message)
    container.appendChild(user)
    item.appendChild(container)
    return item
}


form.addEventListener('submit', (event) => {
    event.preventDefault()
    if(input.value) {
        const msjData = {
            msg: input.value,
            id: socket.id
        }
        socket.emit('chat message', msjData)
        input.value = ''
    }
})

socket.on('chat message', ({msg, id}) => {
    messagesContainer.appendChild(createItem(msg, id))
    window.scrollTo(0, document.body.scrollHeight)
})
